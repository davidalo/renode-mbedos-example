#include "mbed.h"

#define BLINKING_RATE_MS     5000


int main()
{
    while (true) {
        printf("Hello from UART\r\n");
        ThisThread::sleep_for(BLINKING_RATE_MS);
    }
}
