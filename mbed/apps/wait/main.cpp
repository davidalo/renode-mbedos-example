#include "mbed.h"

#define BLINKING_RATE_MS     5000


int main()
{
    while (true) {
        printf("Hello from UART\r\n");
        wait_us(BLINKING_RATE_MS*1000);
    }
}
