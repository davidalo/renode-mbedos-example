# Guide

This project is a demonstration about how to run
and debug Mbed OS applications over Renode.

## Getting started

This section explains how to prepare the 
expected environment to run the example.

### Renode

This section explains how to build and run 
Renode using Docker.

1. Fetch repository using submodule

```bash
git submodule update --init
```

2. Build docker

```bash
USER=$(whoami)

(cd renode-docker && docker build --build-arg userId=$(id -u ${USER}) --build-arg groupId=$(id -g ${USER}) -t renode . && cd ..)
```

3. Run docker using local XAUTHORITY

```bash
USER=$(whoami)

docker run -ti -p 3333:3333 -u $(id -u ${USER}):$(id -g ${USER}) -e DISPLAY -v $XAUTHORITY:/home/${USER}/.Xauthority -v $(pwd)/mbed:/home/developer/mbed --net=host renode
```

### Mbed OS 

This section explains how to build the Mbed OS
example application.

Usage of virtual environments is highly recommended,
you can also use the host mbed os tools. To run 
a virtual environemnt using pipenv you can.

```bash
cd mbed

pipenv install --dev 
pipenv run pip install "mercurial>=5.0"
pipenv shell
```

1. Deploy requirements (from `mbed` folder)

```bash
mbed deploy
```

2. Build application (from `mbed` folder)

```bash
APP=wait

mbed compile -N ${APP} --profile debug --source apps/${APP} --source mbed-os -m NRF52840_DK
```




## Run/debug example

This section explains how to run/debug the Mbed OS
example using Renode.

### Run example

Prerequisites:

1. Renode monitor working.
2. Mbed OS example binaries.

From Renode monitor run:

```bash
$name="nrf52840"

mach create $name

machine LoadPlatformDescription @platforms/cpus/nrf52840.repl

showAnalyzer sysbus.uart0

# wait application
$bin = @/home/developer/mbed/BUILD/NRF52840_DK/GCC_ARM-DEBUG/wait.elf

sysbus LoadELF $bin
start
```

You should see `Hello from UART` in your Analyzer.

### Debug example

Prerequisites:

1. Renode monitor working.
2. Mbed OS example binaries.
3. ARM GDB client available

From Renode monitor run:

```bash
$name="nrf52840"

mach create $name

machine LoadPlatformDescription @platforms/cpus/nrf52840.repl

showAnalyzer sysbus.uart0

# wait application
$bin = @/home/developer/mbed/BUILD/NRF52840_DK/GCC_ARM-DEBUG/wait.elf

sysbus LoadELF $bin
machine StartGdbServer 3333
```

From your host CLI:

```bash
arm-none-eabi-gdb

# Now from the GDB console
file mbed/BUILD/NRF52840_DK/GCC_ARM-DEBUG/wait.elf
target remote :3333

mon start

continue
```

You should see `Hello from UART` in your Analyzer.